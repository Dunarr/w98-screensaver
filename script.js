const canvas = document.getElementById("app")
const ctx = canvas.getContext("2d")


ctx.fillStyle="black"
const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;
canvas.setAttribute("width", WIDTH)
canvas.setAttribute("height", HEIGHT)

const shapes = [{
    color: "#7deeee",
    points:[
        {x: 0,y: 0, speedX: -10, speedY: 10},
        {x: 100,y: 100, speedX: -10, speedY: 10},
        {x: 0,y: 100, speedX: -10, speedY: 10},
        {x: 100,y: 0, speedX: -10, speedY: 10}
    ],
    history:[]
},{
    color: "#d97dee",
    points: [
        {x: 0, y: 0, speedX: 10, speedY: 10},
        {x: 200, y: 200, speedX: 10, speedY: 10},
        {x: 0, y: 200, speedX: 10, speedY: 10},
        {x: 200, y: 0, speedX: 10, speedY: 10}
    ],
    history:[]
},{
    color: "#d1ff00",
    points:[
        {x: 0,y: 100, speedX: 10, speedY: -10},
        {x: 150,y: 100, speedX: 10, speedY: -10},
        {x: 0,y: 100, speedX: 10, speedY: -10},
        {x: 300,y: 0, speedX: 10, speedY: -10}
    ],
    history:[]
}]
function drawShape(shapeToDraw){

    ctx.strokeStyle = shapeToDraw.color;
    for (const path of shapeToDraw.history) {
        ctx.beginPath();
        ctx.moveTo(path[0].x, path[0].y)

        for (let i = 1; i < path.length; i++) {
            ctx.lineTo(path[i].x, path[i].y)
        }
        ctx.lineTo(path[0].x, path[0].y)

        ctx.stroke()
    }


}

function moveShape(shapeToDraw){
    shapeToDraw.history.splice(0,0,shapeToDraw.points.map(({x, y}) => ({x,y})))
    shapeToDraw.history = shapeToDraw.history.slice(0,6)
    for (let i = 0; i < shapeToDraw.points.length; i++) {
        shapeToDraw.points[i].x += shapeToDraw.points[i].speedX
        shapeToDraw.points[i].y += shapeToDraw.points[i].speedY
        if(shapeToDraw.points[i].x >= WIDTH){
            shapeToDraw.points[i].speedX = -10 *Math.random() - 5
        }
        if(shapeToDraw.points[i].x <= 0){
            shapeToDraw.points[i].speedX = 10 *Math.random() +5
        }
        if(shapeToDraw.points[i].y >= HEIGHT){
            shapeToDraw.points[i].speedY = -10*Math.random() - 5
        }
        if(shapeToDraw.points[i].y <= 0){
            shapeToDraw.points[i].speedY = 10*Math.random() +5
        }
    }
}

setInterval(function(){
    ctx.fillRect(0,0, WIDTH, HEIGHT)
    for (const shape of shapes) {
        moveShape(shape)
        drawShape(shape);
    }
}, 50)
